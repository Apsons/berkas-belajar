# TeamB-Backend - Movie Apps

Creating Movie Apps. This app has RESTful endpoint for Movie Apps CRUD operation

# RESTful endpoints

## POST https://calm-plains-90209.herokuapp.com/register 
Register User
```
Request Header : not needed
```
```
Request Body: {
  "fullName": "<user fullName>",
  "email": "<user email>",
  "password": "<user password>",
  "profilePicture": "<user profilePicture>"
}
```
```
Response: (201 - Created){
  "access_token": "<your access token>"
}
```
```
Response: (409 - Conclict){
  "Email already registered!"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST https://calm-plains-90209.herokuapp.com/login 
Login User
```
Request Header : not needed
```
```
Request Body: {
  "email": "<user email>",
  "password": "<user password>"
}
```
```
Response: (201 - OK){
  "access_token": "<your access token>"
}
```
```
Response: (400 - Bad Request){
  "Password incorrect!"
}
```
```
Response: (404 - Not Found){
  "Password incorrect!"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST https://calm-plains-90209.herokuapp.com/admin/movies 
Create Movie
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body: {
  "id": <movie id>,
  "title": "<movie title>",
  "synopsis": "<movie synopsis>",    
  "trailer": "<movie trailer>",
  "poster": "<movie poster>",      
  "genre": "<movie genre>",
  "releaseDate": "<movie releaseDate(yyyy-mm-dd)>",
  "director": "<movie director>",
  "featuredSong": "<movie featuredSong>",
  "budget": <movie budget>
}
```
```
Response: (201 - OK){
  "id": <movie id>,
  "title": "<movie title>",
  "synopsis": "<movie synopsis>",    
  "trailer": "<movie trailer>",
  "poster": "<movie poster>",      
  "genre": "<movie genre>",
  "releaseDate": "<movie releaseDate(yyyy-mm-dd)>",
  "director": "<movie director>",
  "featuredSong": "<movie featuredSong>",
  "budget": <movie budget>,
  "updatedAt": "<movie updatedAt>",
  "createdAt": "<movie createAt>"
}
```
```
Response: (409 - Conclict){
  "Movie already exist!"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE https://calm-plains-90209.herokuapp.com/admin/movies/:MovieId 
Delete Movie
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (202 - Accepted) {
  "1"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/movies/edit/:MovieId 
Edit Movie Form
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK) {
  "id": <movie id>,
  "title": "<movie title>",
  "synopsis": "<movie synopsis>",    
  "trailer": "<movie trailer>",
  "poster": "<movie poster>",      
  "genre": "<movie genre>",
  "releaseDate": "<movie releaseDate(yyyy-mm-dd)>",
  "director": "<movie director>",
  "featuredSong": "<movie featuredSong>",
  "budget": <movie budget>,
  "updatedAt": "<movie updatedAt>",
  "createdAt": "<movie createAt>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT https://calm-plains-90209.herokuapp.com/admin/movies/edit/:MovieId 
Edit Movie
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : {
  "title": "<movie title>",
  "synopsis": "<movie synopsis>",    
  "trailer": "<movie trailer>",
  "poster": "<movie poster>",      
  "genre": "<movie genre>",
  "releaseDate": "<movie releaseDate(yyyy-mm-dd)>",
  "director": "<movie director>",
  "featuredSong": "<movie featuredSong>",
  "budget": <movie budget>
}
```
```
Response: (200 - OK) {
  "1"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/casts 
Casts List
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK) [
  {
    "id": <cast id>,
    "name": "<cast name>",
    "image": "<cast image>",
    "createdAt": "<cast createdAt>",
    "updatedAt": "<cast updateddAt>"
  }
]
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST https://calm-plains-90209.herokuapp.com/admin/casts  
Add Cast
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : {
  "name": "<cast name>",
  "image": "<cast image>"
}
```
```
Response: (201 - Created) [
  {
    "id": <cast id>,
    "name": "<cast name>",
    "image": "<cast image>",
    "createdAt": "<cast createdAt>",
    "updatedAt": "<cast updateddAt>"
  }
]
```
```
Response: (409 - Conflict){
  "<Name already exist!>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/casts/:id  
Cast Edit Form
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (200 - OK) {
  "id": <cast id>,
  "name": "<cast name>",
  "image": "<cast image>",
  "createdAt": "<cast createdAt>",
  "updatedAt": "<cast updateddAt>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT https://calm-plains-90209.herokuapp.com/admin/casts/:id  
Cast Edit 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : {
  "name": "<cast name>",
  "image": "<cast image>"
}
```
```
Response: (200 - OK) [
  1

```
```
Response: (409 - Internal Server Error){
  "<Error Message>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE https://calm-plains-90209.herokuapp.com/admin/casts/:id  
Cast Delete 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (202 - Accepted) [
  1

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE https://calm-plains-90209.herokuapp.com/admin/casts/:id  
Cast Delete 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (202 - Accepted) [
  1

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/movie-cast/:id  
MovieCast List 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (200 - OK) [
  {
    "id": <MovieCast id>,
    "MovieId": "<MovieCast MovieId>",
    "CastId": "<MovieCast CastId>",             
    "createdAt": "<MovieCast CreatedAt>",
    "updatedAt": "<MovieCast UpdateddAt>"
  }

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST https://calm-plains-90209.herokuapp.com/admin/movie-cast/:id  
MovieCast List 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : {
  "MovieId": "<MovieCast MovieId>",
  "CastId": "<MovieCast CastId>"
}
```
```
Response: (201 - Created){
  "id": <MovieCast id>,
  "MovieId": "<MovieCast MovieId>",
  "CastId": "<MovieCast CastId>",             
  "createdAt": "<MovieCast CreatedAt>",
  "updatedAt": "<MovieCast UpdateddAt>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/movie-cast/movie/:movie 
MovieCast List by Movie 
```
Request Header : not needed
```
```
Request Body : not needed
```
```
Response: (200 - OK){
  "Movie": {
    "id": <Movie id>,      
    "title": "<Movie title>",
    "synopsis": "<Movie synopsis>",
    "trailer": "<Movie trailer>",
    "poster": "<Movie poster>",
    "genre": "<Movie genre>",
    "releaseDate": "<Movie releaseDate>",
    "director": "<Movie director>",
    "featuredSong": "<Movie featuredSong>",
    "budget": <Movie budget>,
    "createdAt": "<Movie createdAt>",
    "updatedAt": "<Movie updatedAt>"
    },
    "Cast": [
      {            
        "id": <Cast id>,
        "name": "<Cast name>",
        "image": "<Cast image>",
        "createdAt": "<Cast createdAt>",
        "updatedAt": "<Cast updatedAt>"
      }
    ]
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET https://calm-plains-90209.herokuapp.com/admin/movie-cast/cast/:cast 
MovieCast List by Cast
```
Request Header : not needed
```
```
Request Body : not needed
```
```
Response: (200 - OK){
  "Cast": {
    "id": <Cast id>,
    "name": "<Cast name>",
    "image": "<Cast image>",
    "createdAt": "<Cast createdAt>",
    "updatedAt": "<Cast updatedAt>"
    },
    "Movie": [
      {
        "id": <Movie id>,      
        "title": "<Movie title>",
        "synopsis": "<Movie synopsis>",
        "trailer": "<Movie trailer>",
        "poster": "<Movie poster>",
        "genre": "<Movie genre>",
        "releaseDate": "<Movie releaseDate>",
        "director": "<Movie director>",
        "featuredSong": "<Movie featuredSong>",
        "budget": <Movie budget>,
        "createdAt": "<Movie createdAt>",
        "updatedAt": "<Movie updatedAt>"
      }
    ]
  }

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/admin/movie-cast/edit/:edit 
MovieCast Edit Form
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (200 - OK){
  "id": <MovieCast id>,
  "MovieId": <MovieCast MovieId>,
  "CastId": <MovieCast MovieId>,
  "createdAt": "<MovieCast createdAt>",
  "updatedAt": "<MovieCast updatedAt>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT https://calm-plains-90209.herokuapp.com/admin/movie-cast/edit/:edit 
MovieCast Edit
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : {
  "MovieId": <MovieCast MovieId>,
  "CastId": <MovieCast CastId>
}
```
```
Response: (200 - OK){
  1
}
```
```
Response: (409 - Conflict){
  "Cast already in that movie!"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## DELETE https://calm-plains-90209.herokuapp.com/admin/movie-cast/:id 
MovieCast Delete
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (202 - Accepted){
  1
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/users 
Users List
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK)[
  {
    "id": <user id>,
    "fullName": "<user fullName>",
    "email": "<user email>",
    "password": "<user password>",
    "profilePicture": "<user profilePicture>",
    "role": "<user role>",
    "createdAt": "<user createdAt>",
    "updatedAt": "<user updatedAt>"
  }
]
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/users/edit  
User Edit Form
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK){
  "id": <user id>,
  "fullName": "<user fullName>",
  "email": "<user email>",
  "password": "<user password>",
  "profilePicture": "<user profilePicture>",
  "role": "<user role>",
  "createdAt": "<user createdAt>",
  "updatedAt": "<user updatedAt>"
}

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT https://calm-plains-90209.herokuapp.com/users/edit  
User Edit 
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : {
  "fullName": "<user fullName>",
  "profilePicture": "<user profilePicture>"
}
```
```
Response: (200 - OK)[
  1
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## DELETE https://calm-plains-90209.herokuapp.com/users 
User Delete
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK)[
  1
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}

```
## DELETE https://calm-plains-90209.herokuapp.com/users/delete/:id 
User Delete by Admin
```
Request Header : {
  "access_token": "<your access token">
}
```
```
Request Body : not needed
```
```
Response: (200 - OK)[
  1
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/movies?page=:page 
Movie List
```
Request Header : not needed
```
```
Request Body : {
  
}

```
```
Response: (200 - OK) {
  "maxPage": <maxPage>,
  "previousPage": <previousPage>,
  "nextPage": <nextPage>,
  "result": [
    {
      "id": <Movie id>,      
      "title": "<Movie title>",
      "synopsis": "<Movie synopsis>",
      "trailer": "<Movie trailer>",
      "poster": "<Movie poster>",
      "genre": "<Movie genre>",
      "releaseDate": "<Movie releaseDate>",
      "director": "<Movie director>",
      "featuredSong": "<Movie featuredSong>",
      "budget": <Movie budget>,
      "createdAt": "<Movie createdAt>",
      "updatedAt": "<Movie updatedAt>"
    }
  ]
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST https://calm-plains-90209.herokuapp.com/movies 
Movie List
```
Request Header : not needed
```
```
Request Body : {
    "title": "<Movie title>"
  }
```
```
Response: (200 - OK)[
  {
    "id": <Movie id>,      
    "title": "<Movie title>",
    "synopsis": "<Movie synopsis>",
    "trailer": "<Movie trailer>",
    "poster": "<Movie poster>",
    "genre": "<Movie genre>",
    "releaseDate": "<Movie releaseDate>",
    "director": "<Movie director>",
    "featuredSong": "<Movie featuredSong>",
    "budget": <Movie budget>,
    "createdAt": "<Movie createdAt>",
    "updatedAt": "<Movie updatedAt>"
  }
]

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/genre:genre 
Movie List by Genre
```
Request Header : not needed
```
```
Request Body : 
```
```
Response: (200 - OK)[
  {
    "id": <Movie id>,      
    "title": "<Movie title>",
    "synopsis": "<Movie synopsis>",
    "trailer": "<Movie trailer>",
    "poster": "<Movie poster>",
    "genre": "<Movie genre>",
    "releaseDate": "<Movie releaseDate>",
    "director": "<Movie director>",
    "featuredSong": "<Movie featuredSong>",
    "budget": <Movie budget>,
    "createdAt": "<Movie createdAt>",
    "updatedAt": "<Movie updatedAt>"
  }
]

```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/movies/:id 
Movie List by Id
```
Request Header : not needed
```
```
Request Body : 
```
```
Response: (200 - OK){
  "id": <Movie id>,
  "title": "<Movie title>",
  "synopsis": "<Movie synopsis>",
  "trailer": "<Movie trailer>",
  "poster": "<Movie poster>",
  "genre": "<Movie genre>",
  "releaseDate": "<Movie releaseDate>",
  "director": "<Movie director>",
  "featuredSong": "<Movie featuredSong>",
  "budget": <Movie budget>,
  "createdAt": "<Movie createdAt>",
  "updatedAt": "<Movie updatedAt>"
}
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET https://calm-plains-90209.herokuapp.com/reviews/all  
Reviews List 
```
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (200 - OK)[
    {
        "id": <Review id>,
        "UserId": <Review UserId>,
        "MovieId": <Review MovieId>,
        "review": "<Review review>",
        "rating": <Review Rating>,
        "share": <Review Share>,
        "createdAt": "<Review createdAt>",
        "updatedAt": "<Review updatedAtt>"
    }
```
```
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

# GET https://calm-plains-90209.herokuapp.com/reviews/movie=:MovieId?page=:page 
Review By Movie
```
Request Header : not needed
```
```
Request Body : not needed
```
```
Response: (200 - OK){
  "Movie": "<Movie title>",
  "Rating": <Movie Rating>,
  "Review": [
    "maxPage": <maxPage>,
    "nextPage": <nextPage>,
    "previousPage": <previousPage>,
    "result": [
    {
      "review": "<Review id>",
      "rating": <Review Rating>,
      "User": {
        "fullName": "<User fullName>",
        "profilePicture": "<User profilePicture>"
      }
    }
  ]
}

```
```
Response: (500 - Bad Request){
  "<Error message>"
}
```

# GET https://calm-plains-90209.herokuapp.com/reviews/user 
Review by User
``` 
Request Header : {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response (200 - OK){
  "user": {
    "fullName": "<User fullName>",
    "profilePicture": "<User profilePicture>"
  },
  "reviews": [
    {
      "id": <Review id>,
      "rating": <Review Rating>,
      "share": <Revew share>,
      "Movie": {
        "title": "<Movie title>"
      }
    }
  ]
}

```
```
Response (500 - Bad Request){
  "<Error message>"
}
```

# GET https://calm-plains-90209.herokuapp.com/reviews/share 
Movie Share
```
Request Header: {
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (200 - OK)
{
    "id": "<movie id>",
    "UserId": <user id>,
    "MovieId": 1,
    "review": "Review",
    "rating": "Rating",
    "share": "Share",
    "updatedAt": "Review updatedAt",
    "createdAt": "<Review createdAt>"
}

```
```
Response : (500 - Bad Request){
  "<returned error message>"
}
```

# POST https://calm-plains-90209.herokuapp.com/reviews/:MovieId 
Add Review
``` 
Request Header :{
  "access_token": "<your access token>"
}
```
```
Request Body :{
  "review": "<Review review>", 
  "rating": "<Review rating>",
  "share": "<Review share>"
}
```
```
Response: (409 - Conflict){
  "<Can't review this movie again!>"
}
```

```
Response (200 - OK){
    "id": <Review id>,
    "UserId": <Review UserId>,
    "MovieId": <Review MovieId>,
    "review": "<Review review>",
    "rating": <Review rating>,
    "share": <Review share>,
    "updatedAt": "<Review updatedAt>",
    "createdAt": "<Review createdAt>"
}
```
```
Response: (500 - Bad Request){
  "<Error message>"
}
```

# GET https://calm-plains-90209.herokuapp.com/reviews/:id/edit 
Edit Form Review

``` 
Request Header :{
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response (200 - OK){
    "id": <Review id>,
    "UserId": <Review UserId>,
    "MovieId": <Review MovieId>,
    "review": "<Review review>",
    "rating": "<Review rating>",
    "share": <Review share>,
    "updatedAt": "<Review updatedAt>",
    "createdAt": "<Review createdAt>"
}
```
```
Response: (500 - Bad Request){
  "<Error message>"
}
```


# PUT https://calm-plains-90209.herokuapp.com/reviews/:id/edit 
Edit Review
``` 
Request Header :{
  "access_token": "<your access token>"
}
```
```
Request Body :{
  "review": "<Review review>", 
  "rating": "<Review rating>", 
  "share" : "<Review share>"
}
```
```
Response: (200 - OK){
  1
}

```
```
Response: (500 - Bad Request){
  "<Error message>"
}
```

# DELETE https://calm-plains-90209.herokuapp.com/reviews/:id 
Delete Review
```
Request Header :{
  "access_token": "<your access token>"
}
```
```
Request Body : not needed
```
```
Response: (202 - Accepted){
  1
}
```
```
Response: (500 - Bad Request){
  "<Error message>"
}
```

