const express = require('express');
const app = express();
require('dotenv').config();

const PORT = process.env.PORT || 3000;
const router = require('./routes');

//Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Routes
app.use(router);

//Public Folder
app.use(express.static('./public'));

app.listen(PORT, () => {
    console.log(`Server is running at port : ${PORT}`);
});