/*Check odd or even number
Input = 5 */

var number = 5 ; //isi dengan angka yg mau di test.
if (number % 2 === 0){
    console.log(number + " is Even number.");
}
else if (number % 2 !== 0){
    console.log(number + " is Odd number.");
}

//Print the n first numbers

var number = 15;

for (let a = 1; a <= number; a++) {
    if (a % 3 === 0 && a % 5 === 0) {
        console.log(a + " kelipatan 3 dan 5");
}
    else if (a % 5 === 0) {
        console.log(a + " kelipatan 5");
}
    else if (a % 3 === 0) {
        console.log(a + " kelipatan 3")
}
     else {
        console.log(a)
}
}

/*Print Segitiga
Input = 5*/

for(let a = 0; a<5; a++){
     var bintang = ""
for(let b = 0; b< a+1; b++){
    bintang += "*"
}
console.log(bintang)
}

//Split words without function .split(" ")

var string = "Lorem ipsum is dummy text";
var wadah = [];
var kata = "";

for(let i=0;i<string.length;i++){
    if(string[i] !== " "){
        kata += string[i];
        // console.log(kata); untuk testing.
    }
    else{
        // console.log(kata); untuk testing.
        wadah.push(kata);
        kata = "";
    }

    if(i === string.length - 1){
        wadah.push(kata);
    }
}

console.log(wadah)

/*Find the faktorial
Output 5.4.3.2.1 = 120*/

var number = 5;
var total = 1;
var temp = "";

for (let i = number; i > 0; i--) {
    if (i !== 1) {
        // console.log(i + ".");
        temp += `${i}.`;
    }
    else {
        // console.log(i + " = ")
        temp += `${i} = `;
    }
    total = total * i;
}
console.log(temp, total);

