const Army = require("./Army");
const { Spearman } = require("./Army");

class Barrack {
    constructor(slots){
        this._slots = slots || []
    }

    get slots() {
        return this._slots;
    }

    recruit(Army){
        this._slots.push(Army);
        
    }

    summon(){
        console.log("===Army Arrive===")
        console.log(this._slots);
    }

    disband(name) {
        for (let i = 0; i < this.slots.length; i++) {
            if (this.slots[i].name == name) {
                this.slots.splice(i, 1);
                console.log(`'${name}' GONE`);
            } // coba try lgi..!
        }
    }

    //Grouping
    grouping() {
        let knight = [];
        let spearman = [];
        let archer = [];
        this._slots.forEach(element => {
            switch (element.type) {
                case 'Knight':
                    knight.push(element);
                    break;
                case 'Spearman':
                spearman.push(element);
                    break;
                case 'Archer':
                    archer.push(element);
                    break;                
            }
        });
        let squad = {
            Knight : knight, 
            Spearman : spearman, 
            Archer : archer
        }
        console.log(squad);
    }
}
module.exports = Barrack;