class Army {
    constructor(name, type, level) {
        this._name = name;
        this._type = type;
        this._level = level || 1;
    }

    get name() {
        return this._name;
    }

    get type() {
        return this._type;
    }

    get level() {
        return this._level;
    }

    set setName(nameSet) {
        this._name = nameSet;
    }

    set setType(typeSet) {
        this._type = typeSet;
    }

    set setLevel(levelSet) {
        this._level = levelSet;
    }

    talk () {
        console.log(`${this.name} ${this.type} ${this.level}`);
    }

    training () {
        this._level += 10;
    }
    
}


class Knight extends Army {
    constructor(name, type, level) {
        super (name, type, level);
    }
    talk () {
        super.talk() 
        console.log(`Hallo, I am ${this.name}, I am ${this.type}. Kesatria!!!`)
    }
}

class Spearman extends Army {
    constructor(name, type, level) {
        super (name, type, level);
    }
    talk () {
        super.talk()
        console.log(`Hallo, I am ${this.name}, I am ${this.type}. Penombak!!!`)
    }
}

class Archer extends Army {
    constructor(name, type, level) {
        super (name, type, level);
    }
    talk () {
        super.talk()
        console.log(`Hallo, I am ${this.name}, I am ${this.type}. Pemanah!!!`)
    }
}

let kesatria = new Knight("Baron", "Knight", 1);
let penombak = new Spearman("RAK", "Spearman", 1);
let pemanah = new Archer("Miya", "Archer", 1);

module.exports = {
    Knight, Spearman, Archer
}







