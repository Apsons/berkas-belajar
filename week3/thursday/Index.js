const {Knight, Spearman, Archer} = require ('./Army.js');
const Barrack = require ('./Barrack.js');

let kesatria = new Knight("Baron", "Knight", 1);
let kesatria2 = new Knight("BAM", "Knight", 5);

let penombak = new Spearman("RAK", "Spearman", 1);
let penombak2 = new Spearman("Jalu", "Spearman", 3);

let pemanah = new Archer("Miya", "Archer", 1);
let pemanah2 = new Archer("Irithel", "Archer", 6);

let battalion = new Barrack();

pemanah.talk();
pemanah.training();
pemanah.talk();
kesatria2.talk();
penombak.talk();
console.log(pemanah);

battalion.recruit(pemanah);
battalion.recruit(pemanah);
battalion.recruit(penombak);
battalion.recruit(kesatria);
battalion.summon();
battalion.disband('Miya');
battalion.summon();
battalion.grouping();