class Student {
    constructor(name, age, dateOfBirth, gender, studentId, hobbies) {
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.studentId = studentId;
        this.hobbies = [];
    }

    set setName(nameSet) {
        if (typeof nameSet == 'string') {
            this.name = nameSet
        }
    }

    set setAge(ageSet) {
        if (typeof ageSet == 'number') {
            this.age = ageSet
        }
    }

    set setDateOfBirth(dateOfBirthSet) {
        if (typeof dateOfBirthSet == 'string') {
            this.dateOfBirth = dateOfBirthSet
        }
    }

    set setGender(genderSet) {
        if (genderSet == 'Male' || genderSet == 'Female') {
            this.gender = genderSet
        }
    }

    addHobbies(hobbyAdd) {
        this.hobbies.push(hobbyAdd);
        }
    
    removeHobbies(hobbyDel) {
        for(let i = 0; i < this.hobbies.length; i++){
            if (hobbyDel == this.hobbies[i]) {
                this.hobbies.splice(i, 2);  
            }
        }
    }
    
    get data() {
        return {
            Name: this.name,
            Age: this.age,
            Date_Of_Birth: this.dateOfBirth,
            Gender: this.gender,
            Student_Id: this.studentId, 
            Hobbies: this.hobbies,
        }
    }
}

const siswa = new Student("Bondan", 16);
//console.log(siswa.data);
siswa.name = "Atlasia";
siswa.age = 17;
siswa.dateOfBirth = "14 Juli 2003";
siswa.gender = "Female";
siswa.addHobbies("Main Ayam");
siswa.addHobbies("Memancing Ikan");
siswa.addHobbies("Berenang");
siswa.removeHobbies("Berenang");

console.log(siswa.data);
