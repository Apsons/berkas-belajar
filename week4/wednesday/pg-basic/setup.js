const pool = require('./config/connection');

const productTableSql = `
CREATE TABLE products (
    id SERIAL PRIMARY KEY,
    name VARCHAR (50),
    category VARCHAR (50),
    status VARCHAR (50),
    createdAt VARCHAR (6)
);
`;

pool.query(productTableSql, (err, data) => {
    if (err) {
        throw err;
    } else {
        console.log('Table "product" created successfully');
        pool.end();
    }
});