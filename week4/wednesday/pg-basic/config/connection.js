const { Pool } = require ('pg');


const pool = new Pool({
    database: 'pg-basic',
    host: 'localhost',
    user: 'postgres',
    password: 'hydr4',
    port: 5432
});

module.exports = pool;