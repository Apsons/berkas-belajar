class View {
    static list(data){

        data.forEach(el => {
            console.log(`${el.id}. ${el.task}, status ${el.status}`);
        });
    }

    static message(data){
        console.log(data);
    }
}

module.exports = View;


