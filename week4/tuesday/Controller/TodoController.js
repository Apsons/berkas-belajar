const Todo = require ('../Model/TodoModel');
const TodoView = require ('../View/TodoView');

class TodoController {

    static help(params) {
        const result = Todo.help(params);
        TodoView.help(result);
    }

    static list() {
        const list = Todo.list();
        TodoView.list(list);
    }

    static add(params) {
        const result = Todo.add(params);
        TodoView.message(result);
    }

    static update(params) {
        const result = Todo.update(params);
        TodoView.message(result);
    }

    static delete(params) {
        const result = Todo.delete(params);
        TodoView.message(result);
    }

    static complete(params) {
        const result = Todo.complete(params);
        TodoView.message(result);
    }

    static uncomplete(params) {
        const result = Todo.uncomplete(params);
        TodoView.message(result);
    }

    static message(msg) {
        TodoView.message(msg);
    }
}

module.exports = TodoController;